This project uses composer (https://getcomposer.org/) to manage it's Dependencies.

To use make sure composer is installed:

```
curl -sS https://getcomposer.org/installer | php
```

Then install dependencies

```
php composer.phar install
```

If using mac, homebrew is the recommend installation:  https://getcomposer.org/doc/00-intro.md#globally-on-osx-via-homebrew-

This project uses mongdo for storage.  A configuration file will need to be added:  config/config.ini

Here is an example:

```
[globals]
; Remove next line (if you ever plan to put this app in production)
DEBUG=2
; Where errors are logged
LOGS=../tmp/
; Where the framework will look for templates and related HTML-support files
UI=../views/
; Where uploads will be saved
UPLOADS=assets/
; mongodb url
MONGO_URL="mongodb://localhost:27017"
MONDO_DB=cmc_gl
GITLAB_TOKEN=<Your gitlab token>>>
; Gitlab repo
gitlab_pid="topcoderinc/tc-site"
```

The project is intened to be launched on heroku.  You will need to setup the MongoHQ plugin.

The page is very ugle but function.  Just upload the file and it will update existing issues and create new ones.

There is an example file in the examples/ directory.