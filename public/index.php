<?php

$f3 = require(__DIR__ . '/../vendor/bcosca/fatfree/lib/base.php');

require_once __DIR__ .  "/../vendor/autoload.php";

$f3->config('../config/config.ini');

$f3->route('GET /',
  function() {
      echo Template::instance()->render('layout.htm');
  });

$f3->route('POST /',
  function($f3) {
      $mongodb_url = empty($_ENV['MONGOHQ_URL']) ? $f3->get('MONGO_URL') : $_ENV['MONGOHQ_URL'];
      $mongodb_db = empty($_ENV['MONGOHQ_DB']) ? $f3->get('MONDO_DB') : $_ENV['MONGOHQ_DB'];

      $f3->set('db', new \DB\Mongo($mongodb_url, $mongodb_db));
      $f3->set('gitlab', new \Gitlab\Client('https://gitlab.com/api/v3/'))
        ->authenticate($f3->get('GITLAB_TOKEN'), \Gitlab\Client::AUTH_URL_TOKEN);

      if (Web::instance()->receive('processFile', true, true)) {
        $f3->set('message', 'Issues Updated');
      } else {
        // Something's not right with this uploaded file
        $f3->set('message','Upload failed');
      }

    $f3->reroute('/');
  });

$f3->run();

function processFile($file) {
    global $f3;

    // @TODO when we get more records we will need to batch this
    $current_map = (new \DB\Mongo\Mapper($f3->get('db'), 'id_map'))->find();
    $id_map = array();
    if (is_array($current_map)) {
      foreach ($current_map as $value) {
        $id_map[$value->cmc_id] = $value->gl_id;
      }
    }

    $lexer = new \Goodby\CSV\Import\Standard\Lexer(
      (new \Goodby\CSV\Import\Standard\LexerConfig())->setIgnoreHeaderLine(true));

    $interpreter = new \Goodby\CSV\Import\Standard\Interpreter();
    $f3->set('gitlab_project',  new \Gitlab\Model\Project($f3->get('gitlab_pid'), $f3->get('gitlab')));
    $f3->set('desc_template', file_get_contents('../views/description.htm'));

    $interpreter->addObserver(function(array $columns) use ($f3, $id_map) {
        $hive = array(
          'steps' => $columns[6],
          'expected' => $columns[7],
          'actual' => $columns[8]
        );

        $params = array(
          'title' => $columns[1],
          'description' => Preview::instance()->resolve($f3->get('desc_template'), $hive),
          'labels' => 'import, ' . $columns[9]
        );

        // is the id currently mapped?
        if (isset($id_map[$columns[0]])) {
          $issue = $f3->get('gitlab_project')->updateIssue($id_map[$columns[0]], $params);
        } else {
          $issue = $f3->get('gitlab_project')->createIssue($columns[1], $params);
          $f3->get('db')->id_map->insert(array('cmc_id' => $columns[0], 'gl_id' => $issue->id));
        }
    });
    if ($lexer->parse($file['tmp_name'], $interpreter)) {
      return true;
    } else {
      return false;
    }

}